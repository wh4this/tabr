var globalTABR = undefined;
// Fonction qui permet de lire le fichier et creer le TABR
// n * m = n lignes et m valeurs 
function lireFichierGeneration() {
    // Stockage du fichier de l'utilisateur dans la variable file
    var file = document.getElementById("fileInput").files[0];
    // Si le fichier est correct
    if (file) {
        // Instanciation d'un objet FileReader pour lire le fichier
        var reader = new FileReader();
        // Lecture du fichier au format UTF-8
        reader.readAsText(file, "UTF-8");
        // A la fin de la lecture du fichier, faire
        reader.onload = function (evt) {
            // Verification de l'input ici TODO
            var i = genererTABR(evt.target.result);
            if(i[1].length != 0) {
                var log = "Erreur(s) : \n";
                i[1].forEach(element => {
                    log = log + element + "\n";
                });
                log = log + "TABR : \n";
                log = log + i[0].toString();
                alert(log);
            }
            globalTABR = i[0];
            if(globalTABR != undefined) {
                document.getElementById("tabr").innerHTML = "<h5>TABR Généré</h5><br>" + globalTABR.toHTML();
                document.getElementById("abr").innerHTML = "";
            }
        }
        // Si on rencontre un erreur avec le fichier, faire
        reader.onerror = function (evt) {
            alert('Veuillez recharger le fichier dans le champ d\'importation de fichier');
        }
    }
}

// Fonction qui permet de lire le fichier pour verifier si c'est bon
function lireFichierVerification() {
    // Stockage du fichier de l'utilisateur dans la variable file
    var file = document.getElementById("fileInput").files[0];
    // Si le fichier est correct
    if (file) {
        // Instanciation d'un objet FileReader pour lire le fichier
        var reader = new FileReader();
        // Lecture du fichier au format UTF-8
        reader.readAsText(file, "UTF-8");
        // A la fin de la lecture du fichier, faire
        reader.onload = function (evt) {
            verifierTABR(evt.target.result);
        }
        // Si on rencontre un erreur avec le fichier, faire
        reader.onerror = function (evt) {
            alert('Veuillez recharger le fichier dans le champ d\'importation de fichier');
        }
    }
}
