// n lignes * mcarré pour l'ajout
function genererTABR(contenu) { 
    var textTabr = [];
    var newTabr = new TABR();
    var lignesIncorrects = [];
    var cptLigne = 0;

    var lignes = contenu.split('\r\n');
    lignes.forEach(ligne => {
        cptLigne++;
        var tabr = ligne.split(";");

        // génère un tableau de valeur en fonction de la chaine de caractère contenu
        var intervalle = tabr[0].split(":").map(function(item) {
            return parseInt(item, 10);
        });

        var val = tabr[1].split(":").map(function(item) {
            return parseInt(item, 10);
        }); 
        if(!newTabr.verification(intervalle[0], intervalle[1], val)) {
            lignesIncorrects.push("Ligne " + cptLigne + " : paramètres incorrects");
        }
        newTabr.ajouterIntervalleABR(intervalle[0], intervalle[1], val);
    });
    
    return [newTabr, lignesIncorrects];
}

// n lignes * mcarré pour l'ajout
function verifierTABR(contenu) { 
    var textTabr = [];
    var newTabr = new TABR();
    var lignesIncorrects = [];
    var cptLigne = 0;

    var lignes = contenu.split('\r\n');
    lignes.forEach(ligne => {
        cptLigne++;
        var tabr = ligne.split(";");

        // génère un tableau de valeur en fonction de la chaine de caractère contenu
        var intervalle = tabr[0].split(":").map(function(item) {
            return parseInt(item, 10);
        });

        var val = tabr[1].split(":").map(function(item) {
            return parseInt(item, 10);
        }); 
        if(!newTabr.verification(intervalle[0], intervalle[1], val)) {
            lignesIncorrects.push("Ligne " + cptLigne + " : paramètres incorrects");
        }
        newTabr.ajouterIntervalleABR(intervalle[0], intervalle[1], val);
    });
    if (lignesIncorrects.length == 0) {
        alert("Fichier correct.");
    } else {
        var res = "Fichier incorrect, erreur(s) : \n";
        lignesIncorrects.forEach(element => {
            res = res + element + "\n";
        });
        alert(res);
    }
}
