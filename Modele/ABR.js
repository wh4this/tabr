class ABR {
  constructor(val) {
    if (val == undefined) {
      console.log('Il faut mettre une valeur racine en parametre');
    } else {
      this.val = val;
      this.sag = null;
      this.sad = null;
    }
  }

  // 1
  estFeuille() {
    if(this.sag != null) {
      if(this.sag.val == 'Vide') {
        this.sag = null;
      }
    }
    if(this.sad != null) {
      if(this.sad.val == 'Vide') {
        this.sad = null;
      }
    }
    return this.val != null & this.sag == null & this.sad == null;
  }

  // 1
  estVide() {
    return this.val == 'Vide';
  }

  // n au pire et 1 au mieux
  plusGrand() {
    if(this.sad == null) {
      return this;
    } else if (this.sad.val == 'Vide') {
      this.sad = null;
      return this;
    } else {
      return this.sad.plusGrand();
    }
  }

  // n au pire et 1 au mieux
  plusPetit() {
      if(this.sag == null) {
        return this;
      } else if (this.sag.val == 'Vide') {
        this.sag = null;
        return this;
      } else {
        return this.sag.plusPetit();
      }
    }

  // n au pire et 1 au mieux
  ajouter(val) {
    if (this.val == 'Vide') {
      this.val = val;
      return true;
    } else {
      if (this.val >= val) {
        if (this.sag != null) {
          return this.sag.ajouter(val);
        } else {
          this.sag = new ABR(val);
          return true;
        }
      } else if (this.val < val) {
        if (this.sad != null) {
          return this.sad.ajouter(val);
        } else {
          this.sad = new ABR(val);
          return true;
        }
      }
    }
    return false;
  }

  // n au pire et 1 au mieux (1 + 1)
  supprimer(val) {
    if (this.val == val) {
      if (this.estFeuille()) {
        this.val = 'Vide';
      } else {
        this.deplacer();
      }
      return true;
    } else {
      if (this.val > val) {
        if(this.sag != null) {
          return this.sag.supprimer(val);
        }
      } else if (this.val < val) {
        if(this.sad != null) {
          return this.sad.supprimer(val);
        }
      }
    }
    return false;
  }

  // 1 au mieux et n au pire
  deplacer() {
    if (this.sag == null) {
      this.val = this.sad.val;
      if (this.sad.sag == null) {
        this.sag = null;
      } else {
        this.sag = this.sad.sag;
      }
      if (this.sad.sad == null) {
        this.sad = null;
      } else {
        this.sad = this.sad.sad;
      }
    } else {
      if(this.sad == null) {
        this.val = this.sag.val;
        if (this.sag.sad == null) {
          this.sad = null;
        } else {
          this.sad = this.sag.sad;
        }
        if (this.sag.sag == null) {
          this.sag = null;
        } else {
          this.sag = this.sag.sag;
        }
      } else {
        var noeudPlusPetit = this.sad.plusPetit();
        var valeur = noeudPlusPetit.val;
        this.supprimer(noeudPlusPetit.val);
        this.val = valeur;
      }
    }
  }

  // n au pire et 1 au mieux
  insererABRSuperieur(abr) {
    if (this.val == 'Vide') {
      this.val = abr.val;
      this.sag = abr.sag;
      this.sad = abr.sad;
    } else {
        if (this.sad != null) {
          this.sad.insererABRSuperieur(abr);
        } else {
          this.sad = abr;
        }
      }
    }

  // Parcours prefixe qui retourne un tableau de valeurs
  // n au pire et au mieux
  parcours(res) {
    res.push(this.val);
    if (this.sag != null) {
      if(this.sag.val != 'Vide') {
        this.sag.parcours(res);
      }
    }
    if (this.sad != null) {
      if(this.sad.val != 'Vide') {
        this.sad.parcours(res);
      }
    }
    return res;
  }

  // n au pire et au mieux (n+n)
  toString() {
    var tab = [];
    this.parcours(tab);
    var res = "";
    for (let index = 0; index < tab.length; index++) {
      if(tab[index] != 'Vide') {
        res = res + tab[index];
      }
      if(index != tab.length - 1) {
        res = res + ":"
      }
    }
    return res;
  }
}