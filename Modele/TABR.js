class TABR {
  constructor() {
    this.tab = [];
  }

  // ncarré (parcours des valeurs m * ajout dans l'arbre n)
  ajouterIntervalleABR(min, max, vals) {
    if(this.verification(min, max, vals)) {
      if(this.tab.length == 0) {
        var abr = new ABR(vals[0]);
        if(vals.length >= 2) {
          vals.slice(1).forEach(val => {
            abr.ajouter(val);
          }); 
        } 
        this.tab.push(new Case(min, max, abr));
        console.log('ajouterIntervalleABR : Intervalle '+ min + ' à ' + max + ' avec les valeurs ' + vals + ' viennent d\'être ajoutés');
      } else {
        if(this.verification(min, max, vals)) {
          var abr = new ABR(vals[0]);
          if(vals.length >= 2) {
            vals.slice(1).forEach(val => {
              abr.ajouter(val);
            }); 
          } 
          this.tab.push(new Case(min, max, abr));
          this.tab.sort(this.compare);
          console.log('ajouterIntervalleABR : intervalle '+ min + ' à ' + max + ' avec les valeurs ' + vals + ' viennent d\'être ajoutés');
        } else {
          console.log('ajouterIntervalleABR : L\'intervalle ou une partie de l\'intervalle existe déjà');
        }
      }
    }
  }
  
  // n case tabr au pire et 1 au mieux
  supprimerIntervalle(index) {
    if(index >= 0 & index < this.tab.length) {
      if(this.tab.length == 1) {
        this.tab[0] = null;
      } else {
        for (let i = index; i < this.tab.length - 1; i++) {
          this.tab[i] = this.tab[i + 1];
        }
        this.tab.pop();
      }
    } else {
      alert("supprimerIntervalle : index inexistant")
    }
  }

  // 1
  verifValeur(min, max, val) {
    return val >= min & val <= max;
  }

  // N (n cases de tabr + m valeurs a ajouté)
  verification(min, max, vals) {
    if(min > max) {
      return false;
    }
    for (let i = 0; i < this.tab.length; i++) {
      if(min >= this.tab[i].intervalle[0] && max <= this.tab[i].intervalle[1])  { 
        return false;
      } else if (min <= this.tab[i].intervalle[0] && max >= this.tab[i].intervalle[0]) {
        return false;
      } else if (min <= this.tab[i].intervalle[0] && max >= this.tab[i].intervalle[1]) {
        return false;
      } else if (min <= this.tab[i].intervalle[1] && max >= this.tab[i].intervalle[1]) {
        return false;
      }
    }
    for (let j = 0; j < vals.length; j++) {
      if(!this.verifValeur(min, max, vals[j])) {
        return false;
      }
    }
    return true;
  }

  // 1
  compare(a, b) {
    if(a.intervalle[0] > b.intervalle[0] & a.intervalle[0] > b.intervalle[1]) {
      return 1;
    } else if (a.intervalle[0] < b.intervalle[0] & a.intervalle[1] < b.intervalle[1]) {
      return -1;
    } else {
      return 0;
    }
  }

  // n (n cases du tabr)
  inserer(val) { 
    if(this.tab.length == 0) {
      alert('inserer : Il n\'y a pas de case dans le TABR');
      return false;
    } else {
      var inser = false;
      this.tab.forEach(element => {
        if(this.verifValeur(element.intervalle[0], element.intervalle[1], val) & !inser) {
          element.abr.ajouter(val);
          inser = true;
        }
      });
      if(inser) {
        console.log('inserer : Insertion OK');
        return true;
      } else {
        alert('inserer : Pas inséré');
        return false;
      }
    }
  }

  // n (n cases du tabr)
  supprimer(val) {
    var suppr = false;
    var existe = false;
    var cptElement = 0;
    this.tab.forEach(element => {
      if(this.verifValeur(element.intervalle[0], element.intervalle[1], val) & !suppr) {
        existe = element.abr.supprimer(val);
        if(element.abr.estVide()) {
          this.supprimerIntervalle(cptElement);
        }
        suppr = true;
      }
      cptElement++;
    });
    if(suppr) {
      if (existe) {
        console.log('supprimer : Suppression OK');
        return true;
      } else {
        alert("supprimer : Pas supprimé car introuvable dans l'arbre");
        return false;
      }
    } else {
      alert("supprimer : Pas supprimé car introuvable dans les intervalles");
      return false;
    }
  }

  // n = n noeuds abr + m pour la supression de case
  fusion(index) {
    if(this.tab.length > 1) {
      if(index >= 0 & index < this.tab.length - 1) {
        this.tab[index].abr.insererABRSuperieur(this.tab[index+1].abr);
        this.tab[index].intervalle[1] = this.tab[index + 1].intervalle[1];
        this.supprimerIntervalle(index + 1);
        return true;
      } else {
        alert('fusion : Mauvais index');
        return false;
      }
    } else {
      alert('fusion : Pas assez de de cases')
      return false;
    }
  }

  // n * i = n * i + n
  toAbr() {
    var abr = null;
    var tabValeurs = [];
    this.tab.forEach(element => {
      tabValeurs = tabValeurs.concat(element.abr.parcours([]));
    });
    if(tabValeurs[0] != null) {
      abr = new ABR(tabValeurs[0]);
    }
    for (let index = 1; index < tabValeurs.length; index++) {
      abr.ajouter(tabValeurs[index])
    }
    return abr;
  }

  // n
  toString() {
    var res = "";
    this.tab.forEach(element => {
      res = res + element.intervalle[0] + ":" + element.intervalle[1] + ";" + element.abr.toString() + "\n";
    })
    return res;
  }

  // n
  toHTML() {
    var res = "";
    this.tab.forEach(element => {
      res = res + element.intervalle[0] + ":" + element.intervalle[1] + ";" + element.abr.toString() + "<br>";
    })
    return res;
  }
}